﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.Storage;
using Vavilon_01.DataModels;
using Newtonsoft.Json;

namespace Vavilon_01.DataModels
{
    [DataContract()]
    public class LogIn
    {
        public LogIn() { }

        public LogIn(string id,
                    string login,
                    string password,
                    string dateRegistry,
                    string dateDeactivation,
                    string status)
        {
            this.Id = id;
            this.Login = login;
            this.Password = password;
            this.DateRegistry = dateRegistry;
            this.DateDeactivation = dateDeactivation;
            this.Status = status;
        }

        /// <summary>
        /// Код 
        /// </summary>
        [DataMember()]
        public string Id { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        [DataMember()]
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        [DataMember()]
        public string Password { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        [DataMember()]
        public string DateRegistry { get; set; }

        /// <summary>
        /// Дата деактивации
        /// </summary>
        [DataMember()]
        public string DateDeactivation { get; set; }

        /// <summary>
        /// Статус учетной записи
        /// </summary>
        [DataMember()]
        public string Status { get; set; }
    }

    public class LogInViewModel
    {
        public ObservableCollection<LogIn> ListLogIn { get; set; } = new ObservableCollection<LogIn>();
        string jsonLogIn = string.Empty;
        Uri uri = new Uri("ms-appx:///DataModels/LogInData.json");

        /// <summary>
        /// Загрузка учетных записей из json-файла
        /// </summary>
        /// <returns></returns>
        public async Task<ObservableCollection<LogIn>> LoadLogIn()
        {
            try
            {
                StorageFile logInJsoFile = await StorageFile.GetFileFromApplicationUriAsync(uri);
                jsonLogIn = await FileIO.ReadTextAsync(logInJsoFile);
            }
            catch (Exception ex)
            {
                var s = ex.Message;
            }

            if (jsonLogIn != null)
            {
                ListLogIn = JsonConvert.DeserializeObject<ObservableCollection<LogIn>>(jsonLogIn);
            }
            return ListLogIn;
        }
    }
}


