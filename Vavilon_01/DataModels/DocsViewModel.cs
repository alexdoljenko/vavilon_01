﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Newtonsoft.Json;
using Vavilon_01.DataModels;

namespace Vavilon_01.DataModels
{
    [DataContract()]
    public class Docs
    {
        public Docs() { }

        public Docs(string id,
                    string title,
                    string content,
                    string date,
                    string imagePath)
        {
            this.Id = id;
            this.Title = title;
            this.Content = content;
            this.Date = date;
            this.ImagePath = imagePath;
        }

        /// <summary>
        /// Код 
        /// </summary>
        [DataMember()]
        public string Id { get; set; }

        /// <summary>
        /// Заголовок
        /// </summary>
        [DataMember()]
        public string Title { get; set; }

        /// <summary>
        /// Содержание
        /// </summary>
        [DataMember()]
        public string Content { get; set; }

        /// <summary>
        /// Дата документа
        /// </summary>
        [DataMember()]
        public string Date { get; set; }

        /// <summary>
        /// Путь к изображению
        /// </summary>
        [DataMember()]
        public string ImagePath { get; set; }
    }
}

    public class DocsViewModel
    {
    public ObservableCollection<Docs> ListDocs { get; set; } = new ObservableCollection<Docs>();
    string jsonDocs = string.Empty;
    Uri uri = new Uri("ms-appx:///DataModels/DocsData.json");

    /// <summary>
    /// Загрузка документов из json-файла
    /// </summary>
    /// <returns></returns>
    public async Task<ObservableCollection<Docs>> LoadDocs()
    {
        try
        {
            StorageFile docsJsoFile = await StorageFile.GetFileFromApplicationUriAsync(uri);
            jsonDocs = await FileIO.ReadTextAsync(docsJsoFile);
        }
        catch (Exception ex)
        {
            var s = ex.Message;
        }

        if (jsonDocs != null)
        {
            ListDocs = JsonConvert.DeserializeObject<ObservableCollection<Docs>>(jsonDocs);
        }
        return ListDocs;
    }
}

