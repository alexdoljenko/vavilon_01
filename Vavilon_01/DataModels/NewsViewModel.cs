﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.Storage;
using Newtonsoft.Json;

namespace Vavilon_01.DataModels
{
    [DataContract()]
    public class News
    {
        public News() { }

        public News(string id,
                    string title,
                    string content,
                    string date,
                    string imagePath)
        {
            this.Id = id;
            this.Title = title;
            this.Content = content;
            this.Date = date;
            this.ImagePath = imagePath;
        }

        /// <summary>
        /// Код 
        /// </summary>
        [DataMember()]
        public string Id { get; set; }

        /// <summary>
        /// Заголовок
        /// </summary>
        [DataMember()]
        public string Title { get; set; }

        /// <summary>
        /// Содержание
        /// </summary>
        [DataMember()]
        public string Content { get; set; }

        /// <summary>
        /// Дата новости
        /// </summary>
        [DataMember()]
        public string Date { get; set; }

        /// <summary>
        /// Путь к изображению
        /// </summary>
        [DataMember()]
        public string ImagePath { get; set; }
    }

    public class NewsViewModel
    {
        public ObservableCollection<News> ListNews { get; set; } = new ObservableCollection<News>();
        string jsonNews = string.Empty;
        Uri uri = new Uri("ms-appx:///DataModels/NewsData.json");

        /// <summary>
        /// Загрузка новостей из json-файла
        /// </summary>
        /// <returns></returns>
        public async Task<ObservableCollection<News>>  LoadNews()
        {
            try
            {
                StorageFile newsJsoFile = await StorageFile.GetFileFromApplicationUriAsync(uri);
                jsonNews =  await FileIO.ReadTextAsync(newsJsoFile);
            }
            catch (Exception ex)
            {
               var s = ex.Message;
            }

            if (jsonNews != null)
            {
                ListNews = JsonConvert.DeserializeObject<ObservableCollection<News>>(jsonNews);
            }
            return ListNews; 
        }
    }
}


    