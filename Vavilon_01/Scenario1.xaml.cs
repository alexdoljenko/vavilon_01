﻿using System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using NewsViewModel = Vavilon_01.DataModels.NewsViewModel;
using Vavilon_01.Vews;

// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace Vavilon_01
{
    

    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class Scenario1 : Page
    {
        private MainPage rootPage;

        private NewsViewModel newsVM = new NewsViewModel();

        //private static bool optedIn = false;

        public Scenario1()
        {
            this.InitializeComponent();
      
            // Кэширование страницы
            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        private void Scenario1_BackRequested(object sender, BackRequestedEventArgs e)
        {
            throw new NotImplementedException();
        }

       

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            rootPage = MainPage.Current;

            // Формировадние данных для списка новостей
            ListBoxNews.ItemsSource = await newsVM.LoadNews();

            Frame rootFrame = Window.Current.Content as Frame;

            if (rootFrame.CanGoBack)
            {
                // If we have pages in our in-app backstack and have opted in to showing back, do so
                //Если в стеке backstack нашего приложения имеются страницы, 
                // то необходимо показать кнопку  «Обратно» для возврата на предыдущую страницу.
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility =
                    AppViewBackButtonVisibility.Visible;
            }
            else
            {
                // Remove the UI from the title bar if there are no pages in our in-app back stack
                //Если в стеке backstack нашего приложения нет страниц, 
                // то необходимо спрятать кнопку  «Обратно» .
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility =
                    AppViewBackButtonVisibility.Collapsed;
            }
        }

        private void ListBoxNews_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            var news = (sender as ListBox).SelectedItems[0];
            
            // Navigate to the next page, with info in the parameters whether to enable the title bar UI or not.
            // Перейдите к следующей странице с информацией в параметрах, о деталях новости
            rootFrame.Navigate(typeof(NewsDetail), news);
        }
        

        private void ItemListView_OnItemClick(object sender, ItemClickEventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
