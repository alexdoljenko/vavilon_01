﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Windows.Storage;
using Newtonsoft.Json;
using Vavilon_01.DataModels;

namespace Vavilon_01.ViewModel
{
    public class NewsViewModel
    {
        public ObservableCollection<News> ListNews { get; set; } = new ObservableCollection<News>();

        private string jsonNews;
        private string pathJsonNews = @"ms-appx:\\DataModels\NewsData.json";

        public async Task LoadNews()
        {
            Uri uri = new Uri("ms-appx:///DataModels/NewsData.xml");

            try
            {
                XmlSerializer newsSer = new XmlSerializer(typeof(ObservableCollection<News>));
                FileStream newsStream = new FileStream(@"DataModels\NewsData.xml", FileMode.Open);
                ListNews = (ObservableCollection<News>) newsSer.Deserialize(newsStream);
            }
            catch (Exception ex)
            {
                var e = ex.Message;
                throw;
            }

            ListNews = JsonConvert.DeserializeObject<ObservableCollection<News>>(jsonNews);
        }
    }
}
