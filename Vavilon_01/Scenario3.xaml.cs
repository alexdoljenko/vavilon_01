﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Vavilon_01.Vews;

// Шаблон элемента пустой страницы задокументирован по адресу http://go.microsoft.com/fwlink/?LinkId=234238

namespace Vavilon_01
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class Scenario3 : Page
    {
        private MainPage rootPage;
        private DocsViewModel docsVM = new DocsViewModel();
        public Scenario3()
        {
            this.InitializeComponent();

            // Я хочу, чтобы эта страница всегда кэшировалась так, что нам не нужно добавить логику для сохранения/восстановления состояния 
            this.NavigationCacheMode = NavigationCacheMode.Required;
        }
        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            rootPage = MainPage.Current;
            ListBoxDocs.ItemsSource = await docsVM.LoadDocs();

            //Frame rootFrame = Window.Current.Content as Frame;
            //if (rootFrame.CanGoBack)
            //{
            //    // If we have pages in our in-app backstack and have opted in to showing back, do so
            //    SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            //}
            //else
            //{
            //    // Remove the UI from the title bar if there are no pages in our in-app back stack
            //    SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            //}
        }



        private void ListBoxNews_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            var docs = (sender as ListBox).SelectedItems[0];

            // Navigate to the next page, with info in the parameters whether to enable the title bar UI or not.
            //Перейдите к следующей странице с информацией в параметрах,
            //следует ли включить заголовок интерфейс панели или нет
            rootFrame.Navigate(typeof(DocsDetail), docs);
        }
    }
}
