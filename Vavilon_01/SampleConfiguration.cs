﻿using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using SDKTemplate;

namespace Vavilon_01
{
    public partial class MainPage : Page
    {
        public const string FEATURE_NAME = "Ваш выбор!";

        List<Scenario> scenarios = new List<Scenario>
        {
            new Scenario() { Title="Новости и объявления", ClassType=typeof(Scenario1)},
            new Scenario() { Title="Личный кабинет", ClassType=typeof(Scenario2)},
            new Scenario() { Title="Документы и отчеты", ClassType=typeof(Scenario3)},
            new Scenario() { Title="Обращение в ТСЖ", ClassType=typeof(Scenario4)},
            new Scenario() { Title="Работа ТСЖ", ClassType=typeof(Scenario5)},
            //new Scenario() { Title="Показания счетчиков", ClassType=typeof(Scenario5)},
            new Scenario() { Title="Контакты", ClassType=typeof(Scenario6)},
        };

        #region Custom title bar
        CustomTitleBar customTitleBar = null;

        public void AddCustomTitleBar()
        {
            if (customTitleBar == null)
            {
                customTitleBar = new CustomTitleBar();
                customTitleBar.EnableControlsInTitleBar(areControlsInTitleBar);

                // Make the main page's content a child of the title bar,
                // and make the title bar the new page content.
                UIElement mainContent = this.Content;
                this.Content = null;
                customTitleBar.SetPageContent(mainContent);
                this.Content = customTitleBar;
            }
        }

        public void RemoveCustomTitleBar()
        {
            if (customTitleBar != null)
            {
                // Take the title bar's page content and make it
                // the window content.
                this.Content = customTitleBar.SetPageContent(null);
                customTitleBar = null;
            }
        }

        bool areControlsInTitleBar = false;

        public bool AreControlsInTitleBar
        {
            get
            {
                return areControlsInTitleBar;
            }
            set
            {
                areControlsInTitleBar = value;
                if (customTitleBar != null)
                {
                    customTitleBar.EnableControlsInTitleBar(value);
                }
            }
        }

        #endregion

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void ScenarioControl_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            throw new NotImplementedException();
        }
    }

    public class Scenario
    {
        public string Title { get; set; }
        public Type ClassType { get; set; }
    }
}
